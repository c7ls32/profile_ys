// HomePage.js
import React, { useState } from "react";
import '../../css/homepage4.scss'


const CoreSkill = () => {
    const [coreSkill,setCoreSkill] = useState([
        {
          title:'Program Languages',
          content:'JavaScript, HTML, CSS, Shell, Perl, Python, C, TypeScript'
        },{
          title:'Frontend Frameworks',
          content:'VueJS, ReactJS, Angular10, Bootstrap, Material UI'
        },{
          title:'Database Servers',
          content:'SQL, MongoDB'
        },{
          title:'Testing',
          content:'Jasmine(Angular10), Enzyme(ReactJS), ESLint'
        },{
          title:'Others',
          content:'Docker, Nginx, AWS(EC2)'
        }
      ])
      
  return (
    
    <div>
        <div className="mainpage4">
          <div className="pagecenter4">
            <h1 className="corepart">Core Skills</h1>
            <div className="corepartblocks">
              {coreSkill.map((data, index) => (
                <div className="corepartblock" id="" key={index}>
                  <div className="highlightTitle">
                  {data.title}
                  </div>
                  <div className="highlightContect">
                  {data.content}
                  </div>
                </div>
              ))}
              
            </div>
          </div>
        </div>
    </div>
  );
};

export default CoreSkill;
