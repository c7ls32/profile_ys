// HomePage.js
import React, { useState } from "react";
import Header from "../components/header";

import '../css/common.scss'
import '../css/workexperience.scss'
// import '../css/homepage5.scss'

import '../js/homeYusong.js'
import '../js/homeScroll.js'

import CoreSkill from '../components/homepageSection/coreskill.jsx'





const iconColor ="primary:#365a31"

const Resume = () => {
  const education = [{
    title:'Professional Year IT',
    period:'2021.10 - 2023.1',
    institution:'ATMC',
    duty:['Performance Education']
  },{
    title:'M.A. Information Technology',
    period:'2019.9 - 2021.9',
    institution:'University of New South Wales, Sydney, Australia',
    duty:['Related courses: Database, Data Structure and Algorithm, Software Construction (Script Language), Information Retrieval, Front-End Coding.',
    'Academic performance: 73/100']
  },{
    title:'B.A. Information and Computing Science',
    period:'2014.9 - 2018.8',
    institution:'Southwest Minzu University, Chengdu, China',
    duty:['Related courses: Data Structure, Computer Graphics, Digital Image Processing.',
    'Academic performance: 80/100']
  }]
  const workexperience = [{
    title:'Front End Developer (VueJS)',
    company:'ARC Systems',
    location: 'Sydney, Australia',
    period:'2021.10 - 2024.5',
    duty:[
      'Developed a single page application (SPA).',
      'Responsible for delivering solutions in accordance with specifications, maintaining web and mobile applications, collaborating with backend developers via APIs, and innovating new features.',
      'Crafted Android and iOS beta test versions of the app utilizing Cordova and Xcode.',
      'Leveraged third-party APIs, including Google Maps and Calendly, to enhance web applicationfunctionality.',
      'Enhanced website/software performance through efficient code refactoring, employing SCSS, HTML, JS and reusable functions.',
      'Proficient in Version Control and CI/CD toolsets as Git.',
      'Collaborating within an Agile setting utilizing tools such as JIRA, Confluence, Monday.com, and Notion.',
      'Used Zeplin and Figma to achieve designed user interface.'
    ]
  }]

  const relevantProjects = [{
    title:'Building personal website (Python + ReactJS)',
    period:'2024.03 - Present',
    url:'https://aipd.pro',
    notes:'',
    duty:[
      'Responsible for website UI/UX design and full-stack development, utilizing Python and React.js to create and maintain both frontend and backend.',
      'Worked with third-party dependencies and APIs, to integrate PDF generation and email sending functionalities on the main pages and Contact Us page.',
      'Experience with cloud platforms (AWS -EC2) and Nginx.',
      'Managed end-to-end project lifecycle, encompassing domain acquisition, DNS configuration, server deployment, website development, testing, deployment, and SSL, continuous integration and deployment (CI/CD).'
    ]},{
    title:'Building a booking system in Angular10',
    period:'2021.7 - 2021.9',
    // url:'https://aipd.pro',
    notes:'Volunteer',
    duty:[
      'Used Slack as a team collaboration communication tool.',
      'Made schedule in Trello.',
      'Setup backend environment using Docker.',
      'Worked with MongoDB database system.',
      'Achieved Unit Test in Jasmine.',
    ]},{
      title:'Building a shopping website in ReactJS',
      period:'2021.3 - 2021.5',
      // url:'https://aipd.pro',
      // notes:'Volunteer',
      duty:[
        'Used Balsamiq to make interface sketches for interacting with users.',
        'Used Bootstrap and Material UI components to design user interface.',
        'Responsible for connecting to the API, checking API interface with Postman.',
        'Used GitHub to assist team code collaboration.',
        'Tested React Components and UI in Enzyme.',
        'Solve code specification issues through ESLint.'
      ]},{
        title:'Building a social media website in vanilla JS',
        period:'2020.11 - 2020.12',
        // url:'https://aipd.pro',
        // notes:'Volunteer',
        duty:[
          'Designed the whole page with CSS and HTML.',
          'Use JavaScript to manipulate DOM (Document Object Model) to run dynamic effects.',
          'Used AJAX to reduce data exchanged between the server and browser and made the server respond faster.',
          'Proficient in using JSON for data transmission.'
        ]
    }]

  return (
    <div className="page homepage">
      <Header/>
      <div className="headerSpace"></div>
      {/* <div id="section_2"></div> */}
      <div id="section_4">
        <CoreSkill />
        {/* <img src={require('../image/resume/resume1.jpg')} className="resume"/>
        <img src={require('../image/resume/resume2.jpg')} className="resume"/> */}
      </div>
      <section id="section_work_experience">
        <div className="pagecenter4">
            <h1 className="workexperience">WORK EXPERIENCE</h1>
            <div className="workexperience">
              {workexperience.map((data,index) => (
                <div>
                  <h2>{data.title}</h2>
                  <h3>
                    <div className="subtitle">{data.company} </div>
                    <div className="subtitle">{data.location}</div>
                    <div className="subtitle">{data.period}</div>
                  </h3>
                  {data.duty.map((duty,y)=>(
                    <div className="duty">{duty}</div>
                  ))}
                </div>
              ))}
            </div>
          </div>
      </section>
      <section id="section_work_experience">
        <div className="pagecenter4">
            <h1 className="workexperience">EDUCATION</h1>
            <div className="workexperience">
              {education.map((data,index) => (
                <div>
                  <h2>{data.title}</h2>
                  <h3>
                    <div className="subtitle">{data.institution} </div>
                    <div className="subtitle">{data.period}</div>
                  </h3>
                  {data.duty.map((duty,y)=>(
                    <div className="duty">{duty}</div>
                  ))}
                </div>
              ))}
            </div>
          </div>
      </section>
      <section id="section_work_experience">
        <div className="pagecenter4">
            <h1 className="workexperience">RELEVANT PROJECTS</h1>
            <div className="workexperience">
              {relevantProjects.map((data,index) => (
                <div>
                  <h2>{data.title}</h2>
                  <h3>{data.period}</h3>
                  {/* <h3>
                    <div className="subtitle">{data.company} </div>
                    <div className="subtitle">{data.location}</div>
                    <div className="subtitle">{data.period}</div>
                  </h3> */}
                  {data.duty.map((duty,y)=>(
                    <div className="duty">{duty}</div>
                  ))}
                </div>
              ))}
            </div>
          </div>
      </section>
    </div>
  );
};

export default Resume;
