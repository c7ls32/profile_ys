import React, { useState,useEffect } from "react";
import Header from "../components/header";
import {game} from '../js/game.js'

const Game = () => {


    useEffect(() => {  
        game()
    })
  return (
    
    <div>
        <Header/>
        <h1 >Score: <span id="scoreEl" ></span></h1>
        <h1 >Lives: <span id="livesEl"></span></h1>
        <canvas></canvas>
    </div>
  );
};

export default Game;
