import '../image/game/boat.png'
export function game(){
    const scoreEl = document.querySelector('#scoreEl')
    const livesEl = document.querySelector('#livesEl')
    let score=0
    let lives=3
    scoreEl.innerHTML=score
    livesEl.innerHTML=lives
    
    const canvas = document.querySelector('canvas')
    const c = canvas.getContext('2d')
    canvas.width=window.innerWidth
    canvas.height=window.innerHeight
    
    class Boat{
        constructor(){
            this.velocity={
                x:0,
                y:0
            }
            this.position={
                x:100,
                y:100
            }
            // this.width=100
            // this.height=100
            // this.image = null;

            const image=new Image()
            console.log("?")
            image.src='../image/game/boat.png'
            image.onload=()=>{
                this.image=image
                this.width=image.width
                this.height=image.height
                this.position={
                    x:(canvas.width-this.width)/2,
                    y:canvas.height-this.height-150,
                }
            }
            image.onerror = (e) => {
                console.error("Error loading boat image", e);
            };
    
        }
    
        draw(){
            // c.fillRect(this.position.x,this.position.y,this.width,this.height)
            // console.log("?boat???",this.image)
            // if(this.image){
                c.drawImage(this.image,this.position.x,this.position.y,this.width,this.height)
            // }
        }
    
        update(){
            // console.log("?boat",this.image)
            // if(this.image){
                this.draw()
            // }
            //     this.position.x +=this.velocity.x
            // }
        }
    }
    
    class Sea{
        constructor(){
            const image=new Image()
            image.src='./image/game/sea.png'
            image.onload=()=>{
                this.image=image
                this.width=canvas.width
                this.height=canvas.height
                this.position={
                    x:canvas.width - image.width,
                    y:canvas.height-image.height,
                }
            }
        }
    
    
        draw(){
            if(this.image){
                c.drawImage(this.image,this.position.x,this.position.y,this.width,this.height)
            }
        }
    }
    
    class Plane{
        constructor(){
            this.velocity={
                x:-2,
                y:0,
            }
    
            const image=new Image()
            image.src='./image/game/plane.png'
            image.onload=()=>{
                this.image=image
                this.width=image.width
                this.height=image.height
                this.position={
                    x:canvas.width - image.width,
                    y:20,
                }
            }
        }
    
        draw(){
            if(this.image){
                c.drawImage(this.image,this.position.x,this.position.y,this.width,this.height)
                
            }
        }
    
        update(){
            if(this.image){
                this.draw()
                this.position.x +=this.velocity.x
                if(this.position.x<=0){
                    this.position.x=canvas.width - this.image.width
                }
            }
        }
    }
    
    class Parachutist{
        constructor(xPosition){
            this.velocity={
                x:0,
                y:2,
            }
    
            const image=new Image()
            image.src='./image/game/parachutist.png'
            image.onload=()=>{
                this.image=image
                this.width=image.width
                this.height=image.height
                this.position={
                    x:xPosition,
                    y:0,
                }
            }
        }
    
        draw(){
            if(this.image){
                c.drawImage(this.image,this.position.x,this.position.y,this.width,this.height)
                
            }
        }
    
        drop(){
            if(this.image){
                this.draw()
                this.position.y +=this.velocity.y
                if(this.position.y>=boat.position.y&&this.position.y<=boat.position.y+boat.height){
                    if(this.position.x>=boat.position.x&&this.position.x<=boat.position.x+boat.width){
                        score+=10
                        scoreEl.innerHTML=score
                        dropParachutists.splice(dropParachutists.indexOf(this), 1);
                    }
                }
                if(this.position.y>=sea.position.y&&this.position.y<=canvas.height){
                    lives-=1
                    gameOver=true
                    livesEl.innerHTML=lives
                    dropParachutists.splice(dropParachutists.indexOf(this), 1);
                    return
                }
            }
        }
    }
    
    const sea = new Sea()
    const boat = new Boat()
    console.log('>???BENWENEWNNWNEW')
    const plane = new Plane()
    const parachutist = new Parachutist()
    
    let gameOver = false
    
    const keys={
        'ArrowLeft':{
            pressed:false
        },
        'ArrowRight':{
            pressed:false
        }
    }
    
    let startDrop = true
    let isPaused = false 
    const dropParachutists = []
    
    function createDrop(){
        // dropParachutists.push(new Parachutist(plane.position.x))
    }
    
    function setDrop(){
        if(startDrop&&!isPaused){
            const dropTime =(Math.floor(Math.random() * 5)+2)*1000;
            startDrop=false
            setTimeout(() => {
                createDrop()
                startDrop=true
                setDrop(); 
            }, dropTime);
        }
    }
    
    function animate(){
        if(gameOver==false){
            console.log("anni")
            requestAnimationFrame(animate)
            c.clearRect(0, 0, canvas.width, canvas.height)
            sea.draw()
            boat.update()
            plane.update()
        
            dropParachutists.forEach(dropParachutist =>{
                dropParachutist.drop()
            })
        
            if(keys.ArrowLeft.pressed &&  boat.position.x>=0){
                boat.velocity.x=-5
            }else if(keys.ArrowRight.pressed&&boat.position.x+boat.width<=canvas.width){
                boat.velocity.x=5
            }else{
                boat.velocity.x=0
            }
        }else{
            isPaused=true
            if(lives<=0){
                livesEl.innerHTML='Game Over'
            }else{
                setTimeout(function() {
                    gameOver=false
                    isPaused=false
                    animate()
                    setDrop()
                }, 2000);
            }
        }
    
    }
    
    animate()
    setDrop()
    
    window.addEventListener('keydown',(event)=>{
        if(event.code=='ArrowLeft'){
            console.log("?")
            keys.ArrowLeft.pressed=true
        }else if(event.code=='ArrowRight'){
            keys.ArrowRight.pressed=true
        }
    })
    
    window.addEventListener('keyup',(event)=>{
        if(event.code=='ArrowLeft'){
            keys.ArrowLeft.pressed=false
        }else if(event.code=='ArrowRight'){
            keys.ArrowRight.pressed=false
        }
    })
}
